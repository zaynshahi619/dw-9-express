//[1,2,1,['a','b'],['a','b'],['a','b']]  to remove duplicate

let arr = [1, 2, 1, ["a", "b"], ["a", "b"], ["a", "b"]];

let arrToStr = arr.map((value, index) => {
  return JSON.stringify(value);
});

let removeDup = [...new Set(arrToStr)];

let output = removeDup.map((value, index) => {
  return JSON.parse(value);
});

console.log(output);

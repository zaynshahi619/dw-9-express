// "nitan"=>"Nitan"

//["n","i","t","a","n"] = > ["N","i","t","a","n"]




let input = ["n","i","t","a","n"] 

let output = input.map((value,index)=>{
    if(index === 0){
        return value.toUpperCase()
    }
    else{
        return value
    }
})

console.log(output)

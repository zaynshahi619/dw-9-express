// we cannot convert all array to obj
// we can only convert if we have array like [["name","nitan"],["age",29],["isMarried",false]]
// here we have array of array 

let arr = [
    ["name","ram"],
    ["age",29]
]

let arrToObj = Object.fromEntries(arr)
console.log(arrToObj)
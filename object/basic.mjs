//array is made by the combination of value
//object is made by the combination of key value pair
//name => key ,"ram" => value
//name:"ram" => property

let obj = {
    name:"ram",
    age: 25,
    isMarried:false

}

//get whole object
console.log(obj)

//get specific element
console.log(obj.name)
console.log(obj.age)
console.log(obj.isMarried)


//change specific element
obj.name = "shyam"
console.log(obj)

//delete specific element

delete obj.age
console.log(obj)
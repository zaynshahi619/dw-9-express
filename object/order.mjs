//in object order doesn't matter

let obj = {
    name:"ram",
    age:25,
    isMarried:false
}

//OR

// let obj = {
//     isMarried:false
//     name:"ram",
//     age:25,
//     
// }


let obj = {
    name: "ram",
    age: 25,
    isMarried : false
}



let objKeys = Object.keys(obj)   //to get array of keys only
console.log(objKeys)

let objValues = Object.values(obj)  // to get array of values only
console.log(objValues)

let objProperties = Object.entries(obj)  // to get array of properties i.e array of array
console.log(objProperties)
let ar1 = [
    1,
    2,
    [3,4],
    [
        [5,6],[7,8]
    ]
]

// let arr2 = ar1.flat(1)  // 1 is to open single array inside an array, 
let arr2 = ar1.flat(2)    // 2 is to open two array inside array
console.log(arr2)
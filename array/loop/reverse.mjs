//all method return something but push, pop, unshift, shift change original array
//while reverse and sort method do both return and change original array

// let ar = ["a","b","c"]

// let ar2 = ar.reverse()
// console.log(ar2)
// console.log(ar)


// let str = "abc"
// let ar1 = str.split("")
// console.log(ar1)
// let ar2 = ar1.reverse()
// console.log(ar2)

// let original = ar2.join("")
// console.log(original)

//OR 
 

// let str = "abc"
// let inputArr = str.split('').reverse().join("")
// console.log(inputArr)


let input = "my name is"

let inputArr = input.split(" ").reverse().join(" ")
console.log(inputArr)
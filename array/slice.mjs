let ar1 = ["a","b","c","d","e"]
//         0     1   2  3    4

// let arSlice = ar1.slice(1,3)  // 1 is the starting index and 3 is end index (end index must be always greater by 1)
// console.log(arSlice)


//if end index is not given then it will slice to the end
// let arSlice = ar1.slice(2) 
// console.log(arSlice)
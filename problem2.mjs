let fun = (input)=>{
    let arrToStr = input.map((value,index)=>{
        return JSON.stringify(value)
    })
    let removeDup = [...new Set(arrToStr)];

    let output = removeDup.map((value, index) => {
        return JSON.parse(value);
      });

      return output
}


let _fun = fun([1, 2, 1, ["a", "b"], ["a", "b"], ["a", "b"]]);
console.log(_fun)

try {
    console.log("hello i am try")
    let error = new Error("error")
    throw error             // the code after throw error is not executed
    console.log("hello")
} catch (error) {

                             // catch block is only executed if try block throws error
    console.log("the error has encountered")
}
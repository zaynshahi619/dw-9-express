//interview question
//null is used to empty variable
// never assign variable by undefined


//null is a primitive data type but output gives as object

let a
console.log(a)
a = 5
console.log(a)
a = undefined    //we do not do like this
console.log(a)


// IMPORTANT!!!!
// setTimeOut is always executed at last

console.log("a")
setTimeout(()=>{
        console.log("i am settimeout")
},2000)
console.log("b")


//js is always synchronous because
// at the line setTimeOut it throws the function to the node(background)
//the node holds the function with timer attach with it
// the function in the background is send to memory queue of background after timer is finish.
//then the function is send back to js
// when all the code is executed in js 
//then setTimeOut function is run at last 




//imp
// what is async function
// ==> anything that sends task to background is called asynchronous function



//event Loop
//it constantly monitor callstack
//if callstack is empty
// it takes the function from memory queue and add it to the call stack
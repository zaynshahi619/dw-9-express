//Primitive
//for every let new memory space is created

//Non Primitive
//in non primitive
//if there is let new memory space is created
//first it checks the new variable is copy of other or not
//it will not create memory for that variable rather it share memory

// primitive
// let a= 3
// let b=a
// a = 5
// console.log(a)
// console.log(b)

//Non-primitive
// let ar1 = [1,2]
// let ar2 = ar1   //ar2 is copy of ar1 so memory share only , no memory allocation
// ar1.push(3)
// console.log(ar1)
// console.log(ar2)

// for  ===

// let a = 1;
// let b = a;
// let c = 1;

// console.log(a === b); //it only compares value
// console.log(a === c);

// let ar1 = [1,2]
// let ar2 = ar1
// let ar3 = [1,2]

// console.log(ar1 === ar2)   // it compares the memory is either same or not
// console.log(ar1 === ar3)

// let ar1 = [1, 2];
// let ar2 = ar1;
// console.log(ar1 === ar2); // shares same memory

// console.log([1,2]===[1,2])  //no memory is allocated then how to compare

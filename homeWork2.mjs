// make a arrow function that takes a input and return its upperCase version

let upperName = (name)=>{
    return name.toUpperCase()
}

let _upperName = upperName("maNiSh")
console.log(_upperName)



// make a arrow function that takes a input and return its lowercase version

let lowerName = (name)=>{
    return name.toLowerCase()
}

let _lowerName = lowerName("ManIsh")
console.log(_lowerName)



// make a arrow function that takes a input and return another string which is a trim version (remove both start and end space)

let trimName = (name)=>{
    return name.trim()

}
let _trimName = trimName("    manish  shahi   ")
console.log(_trimName)



// make a arrow function that takes a input true if the string starts with Bearear else return false
let isBearear = (name)=>{
    return name.startsWith("Bearear")
}

let _isBearear = isBearear("Bearear rear dear ")
console.log(_isBearear)



// make a arrow function that takse a sentence  and return another sentence where all nitan is converted to ram

let replceName = (name)=>{
    return name.replaceAll("nitan","ram")
}

let _replaceName = replceName("nitan is a tutor, nitan teaches in deerwalk")
console.log(_replaceName)



// make a arrow function that takes input and return true if it Contain admin or superAdmin
let arr = ["admin","superAdmin","guest","junior"]
let isAdmin = (name)=>{
    if(name === "admin" || name === "superAdmin"){
        return true
    }
    else{
        return false
    }
}

let _isAdmin = isAdmin("superAdmin")
console.log(_isAdmin)



// make a arrow function that takes input as "    niTAn   " and return   "nitan"

let trimName2 = (name)=>{
        return name.trim()
}
let _trimName2 = trimName2("   nitan    ")
console.log(_trimName2)



// make a arrow function that takes a sentence and return total number of character in that sencence (using string.length)

let sentence = (sent)=>{
        return sent.length
}

let _sentence  = sentence("dichlorodiphenyltrichloroethane")
console.log(_sentence)


